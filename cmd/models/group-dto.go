package models

type GroupDto struct {
	Id          int
	Name        string
	Description string
	FullName    string
	FullPath    string
	Iteration   int
}
