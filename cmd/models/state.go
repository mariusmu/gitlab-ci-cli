package models

type CurrentLevel int64

const (
	None     CurrentLevel = 0
	Group    CurrentLevel = 1
	Project  CurrentLevel = 2
	Pipeline CurrentLevel = 3
	Job      CurrentLevel = 4
)

type State struct {
	CurrentProject  *GitlabProjectDto
	CurrentPipeline *PipelineDTO
	AllProjects     []*GitlabProjectDto
	User            *UserDto
	Level           CurrentLevel
}
