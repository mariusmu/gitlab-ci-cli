package models

import "time"

type JobDto struct {
	JobId         int
	JobName       string
	PipelineId    int
	ProjectName   string
	User          *UserDto
	Status        string
	CreatedAt     *time.Time
	Duration      float64
	FailureReason string
	Stage         string
	Runner        string
	Iteration     int
}
