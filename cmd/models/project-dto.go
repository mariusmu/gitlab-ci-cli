package models

type GitlabProjectDto struct {
	ProjectId         int
	Name              string
	NameWithNamespace string
	Iteration         int
}
