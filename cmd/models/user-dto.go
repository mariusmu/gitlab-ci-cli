package models

type UserDto struct {
	Name     string
	Username string
}
