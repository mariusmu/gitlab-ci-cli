package models

import "time"

type PipelineDTO struct {
	Id        int
	ProjectId int
	Status    string
	Ref       string
	UpdatedAt *time.Time
	Source    string
	Iteration int
}
