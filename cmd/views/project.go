package views

import (
	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab-ci-cli/cmd/models"
	"os"
)

type ProjectView struct {
}

func (up ProjectView) Table(projects []models.GitlabProjectDto, title string, err error) {

	if err != nil {
		println(err.Error())
		return
	}
	if len(title) > 0 {
		println("\n--" + title + "--\n")
	}
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Project name", "Project id", "Location"})
	t.SetStyle(table.StyleBold)

	for _, elem := range projects {
		t.AppendRow(table.Row{
			elem.Iteration, elem.Name, elem.ProjectId, elem.NameWithNamespace,
		})

	}
	t.AppendSeparator()
	t.Render()
}
