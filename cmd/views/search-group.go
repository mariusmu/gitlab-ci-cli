package views

import (
	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab-ci-cli/cmd/models"
	"os"
)

type SearchGroupView struct {
}

func (sv SearchGroupView) Table(groups []models.GroupDto, err error) {

	if err != nil {
		println(err.Error())
		return
	}
	println("\n-- Found groups --\n")
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Group name", "Description", "Group id", "Full name", "Full path"})
	t.SetStyle(table.StyleBold)

	for _, elem := range groups {
		t.AppendRow(table.Row{
			elem.Iteration, elem.Name, elem.Description, elem.Id, elem.FullName, elem.FullPath,
		})

	}
	t.AppendSeparator()
	t.Render()
}
