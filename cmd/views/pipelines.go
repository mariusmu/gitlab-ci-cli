package views

import (
	"github.com/jedib0t/go-pretty/v6/table"
	helper "gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/models"
	svc "gitlab-ci-cli/cmd/services"
	"os"
)

type PipelineView struct {
	pipelineService svc.PipelineService
}

func (pv PipelineView) Table(pipelines []models.PipelineDTO, err error) {
	if err != nil {
		println(err.Error())
		return
	}
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Pipeline id", "Ref", "Updated at", "Status"})
	t.SetStyle(table.StyleBold)

	for _, elem := range pipelines {
		t.AppendRow(table.Row{
			elem.Iteration, elem.Id, elem.Ref, elem.UpdatedAt, helper.GetStatusColor(elem.Status),
		})
	}
	t.AppendSeparator()
	t.Render()
}
