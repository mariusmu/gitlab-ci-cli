package views

import (
	"fmt"
	"github.com/fatih/color"
	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/models"
	svc "gitlab-ci-cli/cmd/services"
	"os"
	"strconv"
)

type JobsView struct {
	svc svc.JobService
}

func (jv JobsView) Table(jobs []models.JobDto, err error) {

	if err != nil {
		println(err.Error())
		return
	}

	var projectName = jobs[0].ProjectName
	var pipelineId = color.CyanString(strconv.Itoa(jobs[0].PipelineId))
	println(fmt.Sprintf("\n-- Project (%s) jobs for pipeline with id %s --\n", projectName, pipelineId))
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"#", "Job name", "Job id", "Stage", "Created at", "Duration in sec", "Status"})
	t.SetStyle(table.StyleBold)

	for _, elem := range jobs {
		t.AppendRow(table.Row{
			elem.Iteration, elem.JobName, elem.JobId, elem.Stage, elem.CreatedAt, elem.Duration, helpers.GetStatusColor(elem.Status),
		})

	}
	t.AppendSeparator()
	t.Render()
}
