package cmd

import (
	"bufio"
	"gitlab-ci-cli/cmd/dispatchers"
	"gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/models"
	"os"
)

func Loop() {
	var reader = bufio.NewReader(os.Stdin)
	var state models.State
	groupDispatcher(state, reader)
	//for {
	//	helpers.PrintTitle("Select a command")
	//	helpers.PrintCommand("1; Group functions")
	//	helpers.PrintCommand("..; end")
	//
	//	if text == "..\n" {
	//		break
	//	}
	//	if text == "1\n" {
	//		state.Level = models.Group
	//		groupDispatcher(state, reader)
	//	}
	//}
}

func groupDispatcher(state models.State, reader *bufio.Reader) {
	for {
		helpers.PrintTitle("Select a command")
		helpers.PrintCommand("1; Select current repo")
		helpers.PrintCommand("2; User projects")
		helpers.PrintCommand("3; Search for group")
		helpers.PrintCommand("..; back")
		var reader = bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		switch text {
		case "..\n":
			return
		case "1\n":
			var err = dispatchers.CurrentProjectDispatcher(state, reader)
			if err != nil {
				println("Could not use current project")
			}
		case "2\n":
			dispatchers.UserProjectDispatcher(state, reader)
		}
	}
}
