package services

type UserService struct {
}

func (u UserService) GetUserInfo() (userName string, uid int, err error) {
	git, err := GitlabClient()
	if err != nil {
		print(err.Error())
	}
	user, _, err := git.Users.CurrentUser()
	if err != nil {
		return "", -1, err
	}

	return user.Username, user.ID, nil
}
