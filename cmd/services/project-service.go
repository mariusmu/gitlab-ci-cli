package services

import (
	"github.com/xanzy/go-gitlab"
	models "gitlab-ci-cli/cmd/models"
	"strings"
)

type ProjectService struct {
	service UserService
}

func (p ProjectService) getProjectsInternal(fetchFunc func(client *gitlab.Client) ([]*gitlab.Project, *gitlab.Response, error)) (projects []models.GitlabProjectDto, err error) {
	c, err := GitlabClient()
	if err != nil {
		println(err.Error())
		return nil, err
	}

	proj, _, err := fetchFunc(c)
	if err != nil {
		return nil, err
	}
	var toReturn []models.GitlabProjectDto
	for idx, project := range proj {
		toReturn = append(toReturn, convertToGitlabProject(project, idx))
	}
	return toReturn, err
}

func (p ProjectService) FetchUserProjects() (dto []models.GitlabProjectDto, err error) {

	return p.getProjectsInternal(func(client *gitlab.Client) ([]*gitlab.Project, *gitlab.Response, error) {
		_, uid, err := p.service.GetUserInfo()
		if err != nil {
			println(err.Error())
		}
		var listOptions = &gitlab.ListProjectsOptions{}
		return client.Projects.ListUserProjects(uid, listOptions)
	})
}

func (p ProjectService) FetchGroupProjects(projectId int) (groups []models.GitlabProjectDto, err error) {
	return p.getProjectsInternal(func(client *gitlab.Client) ([]*gitlab.Project, *gitlab.Response, error) {
		var listOptions = &gitlab.ListGroupProjectsOptions{}
		return client.Groups.ListGroupProjects(projectId, listOptions)
	})
}

func (p ProjectService) FetchProjectForUrl(url string) (oroject *models.GitlabProjectDto, err error) {
	c, err := GitlabClient()
	if err != nil {
		println(err.Error())
		return
	}
	var fetchOptions = &gitlab.GetProjectOptions{}
	proj, _, err := c.Projects.GetProject(RemovePrefixFromGitlabUrl(url), fetchOptions)
	if err != nil {
		return nil, err
	}
	var project = convertToGitlabProject(proj, 1)
	return &project, err
}

func RemovePrefixFromGitlabUrl(projectUrl string) string {
	return strings.Replace(projectUrl, "gitlab.com:", "", 1)
}

func convertToGitlabProject(input *gitlab.Project, iter int) models.GitlabProjectDto {
	return models.GitlabProjectDto{
		ProjectId:         input.ID,
		Name:              input.Name,
		NameWithNamespace: input.NameWithNamespace,
		Iteration:         iter,
	}
}
