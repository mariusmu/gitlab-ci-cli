package services

import (
	"github.com/xanzy/go-gitlab"
	models "gitlab-ci-cli/cmd/models"
	"log"
)

type PipelineService struct {
	service UserService
}

func (p PipelineService) FetchPipelinesForProject(projectId int) (dto []models.PipelineDTO, err error) {

	c, err := GitlabClient()
	if err != nil {
		println(err.Error())
		return
	}

	getProjectPipelineOptions := &gitlab.ListProjectPipelinesOptions{
		ListOptions: gitlab.ListOptions{},
		OrderBy:     gitlab.String("updated_at"),
	}

	pipelines, _, err := c.Pipelines.ListProjectPipelines(projectId, getProjectPipelineOptions)
	if err != nil {
		log.Panic("Could not list project pipelines for project with id ", projectId)
		return nil, err
	}
	var pipelinesDtos []models.PipelineDTO
	for idx, pipeline := range pipelines {
		pipelinesDtos = append(pipelinesDtos, convertToGitlabPipeline(pipeline, idx))
	}

	return pipelinesDtos, nil

}

func convertToGitlabPipeline(input *gitlab.PipelineInfo, iter int) models.PipelineDTO {
	return models.PipelineDTO{
		Id:        input.ID,
		ProjectId: input.ProjectID,
		Status:    input.Status,
		Ref:       input.Ref,
		UpdatedAt: input.UpdatedAt,
		Source:    input.Source,
		Iteration: iter,
	}
}
