package services

import (
	"github.com/xanzy/go-gitlab"
	"gitlab-ci-cli/cmd/models"
)

type GroupService struct {
}

func (g GroupService) SearchGroups(search string) (groups []models.GroupDto, err error) {
	return g.getGroupsProjectsInternal(func(client *gitlab.Client) ([]*gitlab.Group, *gitlab.Response, error) {
		return client.Groups.SearchGroup(search)
	})
}

func (g GroupService) getGroupsProjectsInternal(fetchFunc func(client *gitlab.Client) ([]*gitlab.Group, *gitlab.Response, error)) (groups []models.GroupDto, err error) {
	c, err := GitlabClient()
	if err != nil {
		println(err.Error())
		return
	}
	group, _, err := fetchFunc(c)
	if err != nil {
		return nil, err
	}
	var toReturn []models.GroupDto
	for idx, groupIter := range group {
		toReturn = append(toReturn, convertToGitlabGroups(groupIter, idx))
	}
	return toReturn, nil
}

func convertToGitlabGroups(input *gitlab.Group, iter int) models.GroupDto {
	return models.GroupDto{
		Id:          input.ID,
		Name:        input.Name,
		Description: input.Description,
		FullName:    input.FullName,
		FullPath:    input.FullPath,
		Iteration:   iter,
	}
}
