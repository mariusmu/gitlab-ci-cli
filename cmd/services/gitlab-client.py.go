package services

import (
	"github.com/xanzy/go-gitlab"
	"os"
)

func GitlabClient() (git *gitlab.Client, err error) {
	token := os.Getenv("GITLAB_TOKEN_READ")
	client, err := gitlab.NewClient(token)
	return client, err
}
