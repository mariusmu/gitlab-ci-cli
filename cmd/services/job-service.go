package services

import (
	"github.com/xanzy/go-gitlab"
	models "gitlab-ci-cli/cmd/models"
	"log"
)

type JobService struct {
	service UserService
}

func (p JobService) FetchJobForPipeline(projectId int, pipelineId int) (dto []models.JobDto, err error) {

	c, err := GitlabClient()
	if err != nil {
		println(err.Error())
		return
	}

	listJobOptions := &gitlab.ListJobsOptions{}

	jobs, _, err := c.Jobs.ListPipelineJobs(projectId, pipelineId, listJobOptions)
	if err != nil {
		log.Panic("Could not list jobs for pipeline with id ", pipelineId)
		return nil, err
	}
	var pipelinesDtos []models.JobDto
	for idx, pipeline := range jobs {
		pipelinesDtos = append(pipelinesDtos, convertToGitlabJob(pipeline, idx))
	}

	return pipelinesDtos, nil

}

func convertToGitlabJob(input *gitlab.Job, iter int) models.JobDto {
	return models.JobDto{
		JobId:       input.ID,
		JobName:     input.Name,
		PipelineId:  input.Pipeline.ID,
		ProjectName: input.Project.Name,
		User: &models.UserDto{
			Name:     input.User.Name,
			Username: input.User.Username,
		},
		Status:        input.Status,
		CreatedAt:     input.CreatedAt,
		Duration:      input.Duration,
		FailureReason: input.FailureReason,
		Stage:         input.Stage,
		Runner:        input.Runner.Name,
		Iteration:     iter,
	}
}
