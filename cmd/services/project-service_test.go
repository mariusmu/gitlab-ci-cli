package services

import "testing"

func Test_removePrefixFromGitlabUrl(t *testing.T) {
	var res = RemovePrefixFromGitlabUrl("gitlab.com:mariusmu/testgrp/testproj")
	const expected = "mariusmu/testgrp/testproj"
	if res != expected {
		t.Errorf("Expected " + expected + ". Got " + res)
	}
}
