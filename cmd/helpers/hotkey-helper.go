package helpers

import "golang.design/x/hotkey"

func CtrlSHotKey() (*hotkey.Hotkey, error) {
	hk := hotkey.New([]hotkey.Modifier{hotkey.ModCtrl}, hotkey.KeyS)
	err := hk.Register()
	if err != nil {
		println(err.Error())
	}
	return hk, err
}
