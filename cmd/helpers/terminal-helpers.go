package helpers

import (
	"fmt"
	"github.com/fatih/color"
	"strings"
)

func PrintTitle(title string) {

	for i := 0; i < len(title)+17; i++ {
		print("-")
	}
	println()
	println("	" + color.CyanString(title))
	for i := 0; i < len(title)+17; i++ {
		print("-")
	}
	println()
}

func PrintCommand(input string) {
	var output = strings.Split(input, ";")
	var tab = "\t"
	print("- (" + color.CyanString(output[0]) + ")")
	println(tab + output[1])
}

func PrintHeader(input string) {
	println("")
	println(input)
}

func ClearTerminal() {
	fmt.Print("\033[H\033[2J")
}
