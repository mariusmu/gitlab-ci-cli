package helpers

import (
	spinner2 "github.com/briandowns/spinner"
	"time"
)

func StartSpinner() *spinner2.Spinner {
	var spinner = spinner2.New(spinner2.CharSets[26], 100*time.Millisecond)
	spinner.Start()
	return spinner
}
