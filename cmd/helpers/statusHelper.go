package helpers

import "github.com/jedib0t/go-pretty/v6/text"

func GetStatusColor(status string) string {

	var color = text.FgWhite
	switch status {
	case "failed":
		color = text.FgRed
	case "cancelled":
		color = text.FgRed
	case "skipped":
		color = text.FgRed
	case "created":
		color = text.FgYellow
	case "pending":
		color = text.FgYellow
	case "waiting_for_resource":
		color = text.FgYellow
	case "running":
		color = text.FgBlue
	case "preparing":
		color = text.FgBlue
	case "success":
		color = text.FgGreen
	}
	return color.Sprintf(status)
}
