package helpers

import (
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"os"
	"testing"
)

const testPath = "/tmp/repo-test"

func deleteRepo() {
	os.RemoveAll(testPath)
}

func TestDetermineGitRepoUrl(t *testing.T) {

	const remoteOrigin = "git@gitlab.com/test/test"

	inited, err := git.PlainInit(testPath, false)
	if err != nil {
		t.FailNow()
	}
	os.Chdir(testPath)
	inited.CreateRemote(&config.RemoteConfig{
		Name: "origin",
		URLs: []string{remoteOrigin},
	})
	if err != nil {
		t.FailNow()

	}

	path, err := DetermineGitRepoUrl()
	const expected = "gitlab.com/test/test"
	if path != expected {
		t.Errorf("Excepted " + expected + ". Got " + path)
	}
	deleteRepo()

}
