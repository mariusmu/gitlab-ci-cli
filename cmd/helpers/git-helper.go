package helpers

import (
	"gopkg.in/src-d/go-git.v4"
	"os"
	"strings"
)

func DetermineGitRepoUrl() (string, error) {
	wd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	repo, err := git.PlainOpen(wd)
	if err != nil {
		return "", err
	}
	remotes, err := repo.Remotes()
	if len(remotes) > 1 {
		println("More than one remote exist. Will use the first to determine url of project")
	}
	var baseUrl = remotes[0].Config().URLs[0]
	return strings.TrimPrefix(baseUrl, "git@"), nil

}
