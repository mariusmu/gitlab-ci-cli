package dispatchers

import (
	"bufio"
	"github.com/fatih/color"
	"gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/models"
	"gitlab-ci-cli/cmd/services"
	"gitlab-ci-cli/cmd/views"
	"os"
	"strconv"
	"strings"
	"time"
)

func getAndPrintPipelines(state models.State) ([]models.PipelineDTO, error) {
	helpers.PrintTitle("Select a pipeline project " + state.CurrentProject.Name)
	var spinner = helpers.StartSpinner()
	pipelines, err := services.PipelineService{}.FetchPipelinesForProject(state.CurrentProject.ProjectId)
	spinner.Stop()
	views.PipelineView{}.Table(pipelines, err)
	return pipelines, err
}

func PipelineCommands(state models.State, reader *bufio.Reader) {
	for {
		helpers.ClearTerminal()
		pipelines, _ := getAndPrintPipelines(state)
		helpers.PrintHeader("Select a command for pipeline")
		helpers.PrintCommand("number; Show project pipelines for project")
		helpers.PrintCommand("w; Watch pipelines")
		helpers.PrintCommand("..; go back")
		var reader = bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')

		if text == "..\n" {
			return
		} else if text == "w\n" {
			hk, err := helpers.CtrlSHotKey()
			if err != nil {
				continue
			}
			for {
				var breakout = false
				select {
				case <-hk.Keydown():
					{
						helpers.ClearTerminal()
						breakout = true
						break
					}
				case <-time.After(10 * time.Second):
					{
						helpers.ClearTerminal()
						_, err := getAndPrintPipelines(state)
						println()
						println(color.YellowString("Will fetch pipelines every 5 second until you hit CTRL + s"))
						if err != nil {
							println(err.Error())
							continue
						}
					}
				}
				if breakout {
					err := hk.Unregister()
					if err != nil {
						println(err.Error())
					}
					break
				}
			}

		} else {
			number, err := strconv.Atoi(strings.Replace(text, "\n", "", 1))
			if err != nil {
				println(color.HiRedString("Selection from list must be a number"))
				continue
			}
			if number > len(pipelines) || number < 0 {
				println(color.HiRedString("You selected a pipeline outside the range of the list"))
				continue
			}
			state.CurrentPipeline = &pipelines[number]
			jobs, err := services.JobService{}.FetchJobForPipeline(state.CurrentProject.ProjectId, state.CurrentPipeline.Id)
			if err != nil {
				println(err.Error())
				continue
			}
			views.JobsView{}.Table(jobs, err)
		}
	}
}
