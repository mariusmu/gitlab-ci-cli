package dispatchers

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/fatih/color"
	"gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/models"
	"gitlab-ci-cli/cmd/services"
	"gitlab-ci-cli/cmd/views"
	"strconv"
	"strings"
)

func UserProjectDispatcher(state models.State, reader *bufio.Reader) {
	for {
		fmt.Print("\033[H\033[2J")
		var spinner = helpers.StartSpinner()
		projects, err := services.ProjectService{}.FetchUserProjects()
		spinner.Stop()

		helpers.PrintTitle("User projects")

		views.ProjectView{}.Table(projects, "", err)

		helpers.PrintHeader("Select a command:")
		helpers.PrintCommand("1-99; Show project pipelines for project")
		helpers.PrintCommand("..; go back")

		text, _ := reader.ReadString('\n')

		if text == "..\n" {
			break
		}

		number, err := strconv.Atoi(strings.Trim(text, "\n"))
		if err != nil {
			println(err.Error())
			println("Selection from list must be a number")
			continue
		}
		project, err := findProject(projects, number)
		if err != nil {
			println(err.Error())
			continue

		}
		state.CurrentProject = project
		projectDispatcher(state, reader)

	}
}

func CurrentProjectDispatcher(state models.State, reader *bufio.Reader) error {
	fmt.Print("\033[H\033[2J")
	path, err := helpers.DetermineGitRepoUrl()
	if err != nil {
		println(err.Error())
		return err
	}
	proj, err := services.ProjectService{}.FetchProjectForUrl(path)
	if err != nil {
		print(err.Error())
		return err
	}
	println(">> Using project" + color.BlueString(" "+proj.Name) + " <<")
	println()
	state.CurrentProject = proj
	projectDispatcher(state, reader)
	return nil
}

func projectDispatcher(state models.State, reader *bufio.Reader) {
	for {
		helpers.PrintTitle("Commands for project " + state.CurrentProject.Name)
		helpers.PrintCommand("1; Show pipelines for project")
		helpers.PrintCommand("2; Open project in webpage")
		helpers.PrintCommand("..; Go back")

		text, _ := reader.ReadString('\n')

		if text == "..\n" {
			state.CurrentProject = nil
			return
		}
		if text == "1\n" {
			PipelineCommands(state, reader)
		}
	}

}

func findProject(projects []models.GitlabProjectDto, number int) (*models.GitlabProjectDto, error) {
	for _, project := range projects {
		if project.Iteration == number {
			return &project, nil
		}
	}
	return nil, errors.New("selected number is not within the list")
}
