package dispatchers

import (
	"bufio"
	"gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/models"
	"gitlab-ci-cli/cmd/services"
	"gitlab-ci-cli/cmd/views"
	"os"
	"strconv"
)

func JobCommands(state models.State, reader *bufio.Reader) {
	for {
		helpers.ClearTerminal()
		helpers.PrintTitle("Select a pipeline project " + state.CurrentProject.Name)
		var spinner = helpers.StartSpinner()
		pipelines, err := services.PipelineService{}.FetchPipelinesForProject(state.CurrentProject.ProjectId)
		spinner.Stop()
		views.PipelineView{}.Table(pipelines, err)
		helpers.PrintHeader("Select a command for pipeline")
		helpers.PrintCommand("number; Show project pipelines for project")
		helpers.PrintCommand("..; go back")
		var reader = bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')

		if text == ".." {
			return
		}
		number, err := strconv.Atoi(text)
		if err != nil {
			println("Selection from list must be a number")
			continue
		}
		println(number)
	}
}
