package main

import (
	"github.com/urfave/cli/v2"
	"gitlab-ci-cli/cmd"
	"gitlab-ci-cli/cmd/helpers"
	"gitlab-ci-cli/cmd/services"
	"gitlab-ci-cli/cmd/views"
	"golang.design/x/hotkey/mainthread"
	"log"
	"os"
	"strconv"
)

func main() {
	mainthread.Init(start)
}

func start() {

	app := &cli.App{
		EnableBashCompletion: true,
		Commands: []*cli.Command{
			{
				Name:    "interactive",
				Aliases: []string{"it"},
				Usage:   "Start interactive session",
				Action: func(*cli.Context) error {
					cmd.Loop()
					return nil
				},
			},
			{
				Name:    "check-repo",
				Aliases: []string{"c"},
				Usage:   "Check repo",
				Action: func(*cli.Context) error {
					curr, _ := helpers.DetermineGitRepoUrl()
					println(curr)
					return nil
				},
			},
			{
				Name:    "user-projects",
				Aliases: []string{"up"},
				Usage:   "List user projects",
				Action: func(*cli.Context) error {
					projects, err := services.ProjectService{}.FetchUserProjects()
					views.ProjectView{}.Table(projects, "User projects", err)
					return nil
				},
			},
			{
				Name:      "search-group",
				Aliases:   []string{"s"},
				Usage:     "Search a group",
				ArgsUsage: "group_url",
				Action: func(ctx *cli.Context) error {
					if ctx.Args().Len() < 1 {
						println("You must supply group url when searching")
						return nil
					}
					var searchPath = ctx.Args().Get(0)
					groups, err := services.GroupService{}.SearchGroups(searchPath)
					views.SearchGroupView{}.Table(groups, err)
					return nil
				},
			},
			{
				Name:      "group-projects",
				Aliases:   []string{"gp"},
				Usage:     "List group projects",
				ArgsUsage: "group_id",
				Action: func(ctx *cli.Context) error {
					if ctx.Args().Len() < 1 {
						println("You must supply group id")
						return nil
					}
					groupId, err := strconv.Atoi(ctx.Args().Get(0))
					if err != nil {
						println("You must supply group id of type int")
						return nil
					}
					groups, err := services.ProjectService{}.FetchGroupProjects(groupId)
					views.ProjectView{}.Table(groups, "Group projects", err)
					return nil
				},
			},
			{
				Name:      "project-pipeline",
				Aliases:   []string{"p"},
				Usage:     "Get pipelines for project",
				ArgsUsage: "project_id",
				Action: func(ctx *cli.Context) error {
					if ctx.Args().Len() == 0 {
						println("You must add a project id")
						return nil
					}
					var firstArg = ctx.Args().Get(0)
					converted, err := strconv.Atoi(firstArg)
					if err != nil {
						println("First argument for project-pipeline command must be a project id (int)")
						return nil
					}
					println("\n-- Project pipeline --\n")
					pipelines, err := services.PipelineService{}.FetchPipelinesForProject(converted)
					views.PipelineView{}.Table(pipelines, err)
					return nil
				},
			},

			{
				Name:      "pipeline-job",
				Aliases:   []string{"j", "jobs"},
				Usage:     "Get pipelines jobs for project",
				ArgsUsage: "project_id pipeline_id",
				Action: func(ctx *cli.Context) error {
					if ctx.Args().Len() < 2 {
						println("You must add a project id and pipeline id")
						return nil
					}
					var firstArg = ctx.Args().Get(0)
					projectId, err := strconv.Atoi(firstArg)
					if err != nil {
						println("First argument for project-pipeline command must be a project id (int)")
						return nil
					}

					var secondArg = ctx.Args().Get(1)
					pipelineId, err := strconv.Atoi(secondArg)

					if err != nil {
						println("First argument for project-pipeline command must be a project id (int)")
						return nil
					}
					jobs, err := services.JobService{}.FetchJobForPipeline(projectId, pipelineId)
					views.JobsView{}.Table(jobs, err)
					return nil
				},
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
